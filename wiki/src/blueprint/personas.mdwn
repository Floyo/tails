[[!meta title="Personas"]]

[[!toc levels=3]]

# Big picture

This is about [[!tails_ticket 11162]].

# External resources about personas

  - [Gus Andrew's User Personas for Privacy and Security](https://medium.com/@gusandrews/user-personas-for-privacy-and-security-a8b35ae5a63b#.8lyxpkom4)
  - [Personas on usability.gov](http://www.usability.gov/how-to-and-tools/methods/personas.html)

# User scenarios

From a discussion in August 2016:

- A small group of English-speaking journalists use Tails to analyse an
  archive of leaked documents and prepare articles about them.

- A well-established music teacher uses Tails to bypass the software and
  network limitations on his professional laptop.

- Someone living in a controlled housing uses Tails to avoid having all
  his Internet browsing monitored by the staff.

- A political scientist in Egypt uses Tails to send his findings to
  Germany while avoiding State surveillance.

- A woman who lives with someone abusing her uses Tails to communicate
  stealthily and without living traces on the home computer.

- A political activist uses Tails to coordinate with their affinity
  group and organize a demonstration.

- A person suffering from cancer uses Tails to learn about their disease
  while avoiding their employer learning about their condition.

- A group of people preparing a plea for defending activists in court
  uses Tails to prepare the plea and store the documents in a safe
  place.

- A free software contributor uses Tails to translate the security tools
  used by their community into Bahasa Indonesia.

- A whistle-blower uses Tails to store and edit to-be-leaked documents
  securely.

- A lawyer uses Tails to communicate with their client in a secure and
  anonymous fashion.

- A Tails developer uses Tails to develop Tails and understand better
  the struggling of users.

- A university student uses Tails to publish publicly-funded but
  copyrighted scientific papers online.

- Union workers use Tails to coordinate about labor struggle over their
  company's network.

- A Russian tourist uses Tails to access their online bank account
  without getting their credential stolen.

- A nomadic person with no personal laptop uses Tails to carry the same
  computing environment and personal documents around.

- A abuse contact uses Tails in order to communicate with survivors
  contacting them.

- A person without their own Internet access uses Tails to use an
  uncensored Internet.

- A teenager uses Tails to escape parental control filter.

- A group of people use Tails to write a book together and publish it.

- A scientist uses Tails to report and transcribe interviews while
  preserving the personal identifying information of the interviewees.

- Webmasters of a cop-watching website use Tails to reduce their chance
  of being caught while reporting on police violence.

- A photographer uses Tails to store and work on pictures before
  publication.

- A person without the need for a big storage uses Tails as their main
  operating system to have more privacy.
